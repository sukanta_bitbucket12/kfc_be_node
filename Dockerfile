FROM node:16

#create app directory
WORKDIR /usr/src/app

#install app dependency
COPY package*.json ./

RUN npm install pm2 -g

#Bundle app source
COPY . .

EXPOSE 3001
CMD ["node", "server.js"]
